# IsItPointBreakDay.com

We've all wondered whether its Point Break day or not. This project seeks to end that mystery once and for all. This is 
a countdown timer that alerts you if it's Point Break day (September 14th) or gives you a countdown to the holiday.

## Usage
This is just an HTML file with some CSS and JavaScript. Use it however you like.

## About the Author(s)
For the countdown logic/display I started with 
[https://codepen.io/doriancami/pen/jEJvaV](https://codepen.io/doriancami/pen/jEJvaV)
I just added support for Days. Why reinvent the wheel, right? Thanks [Dorian Camilleri](https://codepen.io/doriancami)!


I'm a senior web developer with a focus on JavaScript (React, Node) and PHP (Laravel, WordPress, Drupal). I'm always 
happy to chat!